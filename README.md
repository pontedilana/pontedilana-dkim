# POC Verifica DKIM

Questo progetto è una riproduzione del nostro sito web, sviluppato in 
Symfony 6.3 su piattaforma PHP 8.1; è una versione semplificata, ridotta all'osso, 
comprendente solamente la pagina contatti, in cui è presente il form,
che tramite una chiamata ajax richiama il controller `CallToActionController` 
per inviare tramite `symfony\mailer` la mail.

## Installazione
```shell
composer install
yarn install
yarn encore dev
```
## Configurazione ambiente
Le variabili d'ambiente possono essere configurare creando un file `.env.local` 
nella root del progetto, per esempio:

`.env.local`
```dotenv
MAILER_DSN="smtp://username:password@out.smtpfy.com:587"

EMAIL_SENDER_NAME="Nome mittente"
EMAIL_SENDER_ADDRESS="no-reply@example.com"
EMAIL_RECEIVER_ADDRESS="destinatario@example.com"
```

Username e password utilizzati nelle variabili d'ambiente devono essere 
codificati per l'URL, tramite questo semplice script si ottendono le corrette credenziali

```php
<?php

$username = 'username da codificati';
$password = 'password da codificare'

echo 'username': urlencode($username) . "\n"
echo 'password': urlencode($password) . "\n"

```

## Webserver
Per utilizzare il progetto è consigliabile utilizzare `symfony-cli`, strumento ufficiale di Symfony.
Le [istruzioni per installarlo sono qui](https://symfony.com/download) .

Per avviare il progetto bansta un semplice `symfony serve` lanciato dalla root del progetto,
il comando fornirà una url per utilizzare il sito ([https://127.0.0.1:8000/](https://127.0.0.1:8000/))
