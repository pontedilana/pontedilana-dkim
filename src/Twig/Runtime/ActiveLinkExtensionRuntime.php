<?php

namespace App\Twig\Runtime;

use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\RuntimeExtensionInterface;

class ActiveLinkExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    /**
     * Pass route names. If one of route names matches current route (with all parameters), this function returns $classname.
     *
     * @param array<array-key, string>|array<array-key, array{string, array<string, string>}> $routesToCheck
     */
    public function activeLink(array $routesToCheck, string $classname = 'active'): string
    {
        if (null === $mainRequest = $this->requestStack->getMainRequest()) {
            return '';
        }

        $currentRoute = $mainRequest->attributes->get('_route');

        foreach ($routesToCheck as $routeToCheck) {
            if (is_array($routeToCheck)) {
                // Need exactly 2 parameters when array is passed
                if (2 !== count($routeToCheck)) {
                    throw new \RuntimeException('Wrong parameter count: activeLink wants exactly 2 parameters when passing an array');
                }

                [$urlName, $params] = $routeToCheck;
                if ($urlName !== $currentRoute) {
                    continue;
                }

                $allParametersMatch = true;
                foreach ($params as $key => $value) {
                    if ($mainRequest->get($key) !== $value) {
                        $allParametersMatch = false;
                        break;
                    }
                }

                if ($allParametersMatch) {
                    return $classname;
                }
            }

            if ($routeToCheck === $currentRoute) {
                return $classname;
            }
        }

        return '';
    }
}
