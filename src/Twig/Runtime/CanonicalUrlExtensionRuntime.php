<?php

namespace App\Twig\Runtime;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\RuntimeExtensionInterface;

class CanonicalUrlExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly RequestStack $requestStack
    ) {
    }

    public function generateUrl(
        string $route = null,
        array $parameters = null
    ): string {
        $request = $this->requestStack->getCurrentRequest();
        $route = $route ?? $request?->attributes->get('_route');
        $parameters = $parameters ?? $request?->attributes->get('_route_params', []);

        if (!$route) {
            return '';
        }

        $url = $this->urlGenerator->generate($route, $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
        if (str_starts_with($url, 'index.php/')) {
            $url = str_replace('index.php/', '', $url);
        }

        return $url;
    }

    public function renderLinkTag(
        string $href = null
    ): string {
        if (null === $href) {
            $href = $this->generateUrl();
        }

        if (mb_strlen($href) > 0) {
            return sprintf('<link rel="canonical" href="%s">', $href);
        }

        return '';
    }
}
