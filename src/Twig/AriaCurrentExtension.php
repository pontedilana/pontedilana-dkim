<?php

/*
 * This file is part of the Pontedilana Twig package.
 *
 * (c) Manuel Dalla Lana <manuel@pontedilana.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ActiveLinkExtension.
 *
 * Usage:
 *
 * <ul class="nav navbar-nav">
 *   <li class="{{ ariaCurrent['form', 'edit_form']) }}"><a href="{{ path('form') }}">Form</a></li>
 *   <li class="{{ ariaCurrent(['list']) }}"><a href="{{ path('list') }}">List</a></li>
 * </ul>
 */
class AriaCurrentExtension extends AbstractExtension
{
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('ariaCurrent', \Closure::fromCallable($this->ariaCurrent(...))),
        ];
    }

    /**
     * Pass route names. If one of route names matches current route, this function returns 'active'.
     */
    public function ariaCurrent(array $routesToCheck, string $ariacurrent = 'aria-current="page"'): string
    {
        if (null === $currentRequest = $this->requestStack->getCurrentRequest()) {
            return '';
        }

        $currentRoute = $currentRequest->get('_route');

        foreach ($routesToCheck as $routeToCheck) {
            if (is_array($routeToCheck)) {
                // Need exactly 2 parameters when array is passed
                if (2 !== count($routeToCheck)) {
                    throw new \RuntimeException('Wrong parameter count: activeLink wants exactly 2 parameters when passing an array');
                }

                [$urlName, $params] = $routeToCheck;
                if ($urlName !== $currentRoute) {
                    return '';
                }

                foreach ($params as $key => $value) {
                    if ($currentRequest->get($key) !== $value) {
                        return '';
                    }
                }

                return $ariacurrent;
            }

            if ($routeToCheck === $currentRoute) {
                return $ariacurrent;
            }
        }

        return '';
    }
}
