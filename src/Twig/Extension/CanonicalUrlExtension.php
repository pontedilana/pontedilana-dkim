<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\CanonicalUrlExtensionRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CanonicalUrlExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/3.x/advanced.html#automatic-escaping
            new TwigFunction('canonical_url', [CanonicalUrlExtensionRuntime::class, 'generateUrl']),
            new TwigFunction('canonical_link_tag', [CanonicalUrlExtensionRuntime::class, 'renderLinkTag'], ['is_safe' => ['html']]),
        ];
    }
}
