<?php

/*
 * This file is part of the Pontedilana Twig package.
 *
 * (c) Manuel Dalla Lana <manuel@pontedilana.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig\Extension;

use App\Twig\Runtime\ActiveLinkExtensionRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ActiveLinkExtension.
 *
 * Usage:
 *
 * <ul class="nav navbar-nav">
 *   <li class="{{ activeLink(['form', 'edit_form']) }}"><a href="{{ path('form') }}">Form</a></li>
 *   <li class="{{ activeLink(['list']) }}"><a href="{{ path('list') }}">List</a></li>
 *   <li class="{{ activeLink([ ['category_show', {'slug': category.slug}] ])"}}>{{ category.title }}</a>
 * </ul>
 */
class ActiveLinkExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'activeLink',
                [ActiveLinkExtensionRuntime::class, 'activeLink'],
                ['is_safe' => ['html']]
            ),
        ];
    }
}
