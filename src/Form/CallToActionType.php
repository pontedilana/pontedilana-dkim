<?php

namespace App\Form;

use App\DTO\CallToActionDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CallToActionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'nome',
                TextType::class,
                [
                    'label' => 'Il tuo nome',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Il tuo nome',
                        'aria-label' => 'Il tuo nome',
                    ],
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'La tua email',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'La tua email',
                        'aria-label' => 'La tua email',
                    ],
                ]
            )
            ->add(
                'telephone',
                TextType::class,
                [
                    'label' => 'Il tuo cellulare (facoltativo)',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Il tuo cellulare (facoltativo)',
                        'area-label' => 'Il tuo cellulare (facoltativo)',
                    ],
                ]
            )
            ->add(
                'richiesta',
                TextareaType::class,
                [
                    'label' => 'Buongiorno, vorrei avere informazioni in merito a...',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Buongiorno, vorrei avere informazioni in merito a...',
                        'aria-label' => 'Buongiorno, vorrei avere informazioni in merito a...',
                        // 'rows' => 4,
                    ],
                ]
            )
            ->add(
                'consenso',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'label' => 'Dichiaro di aver letto l\'#TERMS_OF_USE_LINK# ed esprimo il mio consenso al trattamento dei dati per le finalità indicate',
                    // 'required' => true,
                    'constraints' => new Assert\IsTrue(),
                ]
            )
            ->add(
                // Questo è un campo fasullo, utilizzato per ingannare i bot
                'priorityList',
                HiddenType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'invio',
                ]
            )
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CallToActionDTO::class,
        ]);
    }
}
