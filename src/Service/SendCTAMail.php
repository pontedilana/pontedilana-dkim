<?php

namespace App\Service;

use App\DTO\CallToActionDTO;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class SendCTAMail
{
    public function __construct(private readonly MailerInterface $mailer, private readonly string $emailSenderName, private readonly string $emailSenderAddress, private readonly string $emailReceiverAddress)
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendEmail(CallToActionDTO $ctaDTO, string $ip): void
    {
        $message = (new TemplatedEmail())
            ->subject('Nuova richiesta di contatto')
            ->from(new Address($this->emailSenderAddress, $this->emailSenderName))
            ->to(new Address($this->emailReceiverAddress))
            ->replyTo(new Address($ctaDTO->email))
            ->htmlTemplate('email/cta.html.twig')
            ->context([
                'ctaDTO' => $ctaDTO,
                'clientIP' => $ip,
            ]);

        $message->getHeaders()->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply');

        $this->mailer->send($message);
    }
}
