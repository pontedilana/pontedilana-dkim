<?php

namespace App\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ContattiConsulenzaController extends AbstractController
{
    #[Route(path: '/', name: 'contatti')]
    public function index(Request $request): Response
    {
        $meta = [
            'title' => 'I migliori rapporti di comunicazione iniziano con una chiaccherata',
            'description' => 'Siamo un\'agenzia di comunicazione che prende le misure e tesse i suoi lavori nel cuore della provincia di Treviso, a Montebelluna',
        ];

        return $this->render('frontend/contatti/index.html.twig', [
            'formProcessed' => $request->get('f'),
            'meta' => $meta,
        ]);
    }

    #[Route(path: '/contatti/grazie', name: 'contatti-grazie')]
    public function redirectOldPage(): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        return $this->redirectToRoute('contatti', [], Response::HTTP_MOVED_PERMANENTLY);
    }
}
