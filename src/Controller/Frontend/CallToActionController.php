<?php

namespace App\Controller\Frontend;

use App\DTO\CallToActionDTO;
use App\Form\CallToActionType;
use App\Service\SendCTAMail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;

class CallToActionController extends AbstractController
{
    #[Route(path: '/form-contatti', name: 'formContatti')]
    public function contatti(Request $request, SendCTAMail $sendCTAMail, string $formProcessed = null): Response
    {
        $ctaForm = $this->createForm(CallToActionType::class);
        $ctaForm->handleRequest($request);

        if ($ctaForm->isSubmitted() && $ctaForm->isValid()) {
            $ctaDTO = $ctaForm->getData();
            $sendCTAMail->sendEmail($ctaDTO, $request->getClientIp());

            return $this->redirect($request->headers->get('referer') . '?f=ok#contatti');
        }

        return $this->render('frontend/common/_cta-form.html.twig', [
            'ctaForm' => $ctaForm,
            'formProcessed' => $formProcessed,
        ]);
    }

    public function contattiThanks(): Response
    {
        return $this->render('frontend/common/_cta-thanks.html.twig');
    }

    #[Route(path: '/api/form-contatti', name: 'formContattiAjax')]
    public function contattiManage(Request $request, SendCTAMail $sendCTAMail): Response
    {
        $ctaForm = $this->createForm(CallToActionType::class);
        $ctaForm->handleRequest($request);
        if ($ctaForm->isSubmitted() && $ctaForm->isValid()) {
            /** @var CallToActionDTO $ctaDTO */
            $ctaDTO = $ctaForm->getData();
            try {
                /* @noinspection StrlenInEmptyStringCheckContextInspection */
                if (0 === mb_strlen((string)$ctaDTO->priorityList)) {
                    $sendCTAMail->sendEmail($ctaDTO, $request->getClientIp());
                }

                return $this->render('frontend/common/_cta-thanks.html.twig');
            } catch (\Exception|TransportExceptionInterface $e) {
                $ctaForm->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('frontend/common/_cta-form.html.twig', [
            'ctaForm' => $ctaForm,
            'formProcessed' => null,
        ]);
    }
}
