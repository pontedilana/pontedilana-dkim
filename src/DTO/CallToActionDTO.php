<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class CallToActionDTO
{
    #[Assert\NotBlank(message: 'Inserire il proprio nome')]
    public string $nome;

    #[Assert\NotBlank(message: 'Inserire la propria email aziendale')]
    #[Assert\Email(message: 'Indirizzo email non valido')]
    public string $email;

    public ?string $telephone = null;

    #[Assert\NotBlank(message: 'Inserire la vostra richiesta')]
    public string $richiesta;

    #[Assert\IsTrue(message: "Accettare l'autorizzazione al trattamento dei dati personali")]
    public bool $consenso;

    // Questo è un campo fasullo, utilizzato per ingannare i bot
    public $priorityList;

    public function getNome(): string
    {
        return $this->nome;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone ?? null;
    }

    public function getRichiesta(): string
    {
        return $this->richiesta;
    }

    public function isConsenso(): bool
    {
        return $this->consenso;
    }
}
