// start the Stimulus application
import './bootstrap.js';

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// import twitter bootstrap js
require('bootstrap');

// scripts
import './js/close-mobile-menu';
import './js/equal-height';
import './js/fontawesome';
import './js/navbar-fixed-top';
