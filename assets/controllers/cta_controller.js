import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
  static targets = ['form']

  static values = {
    formUrl: String,
  }

  onSubmit (event) {
    event.preventDefault()
    this.submitForm(this.formTarget)
  }

  async submitForm (form) {
    const response = await fetch(this.formUrlValue, {
      method: form.method,
      body: new URLSearchParams(new FormData(form)),
    })
    this.element.innerHTML = await response.text()
  }
}
