import {config, dom, library} from '@fortawesome/fontawesome-svg-core'

// regular
import {
    faBars,
    faXmark,
    faCopyright,
    faArrowRight,
    faArrowLeft,
} from '@fortawesome/free-solid-svg-icons'

// brands
import {
    faLinkedin,
    faFacebook,
    faInstagram
} from '@fortawesome/free-brands-svg-icons'

// Change the config to fix the flicker
//config.mutateApproach = 'sync'
config.keepOriginalSource = false

library.add(
    faCopyright,
    faBars,
    faArrowRight,
    faArrowLeft,
    faLinkedin,
    faFacebook,
    faInstagram,
    faXmark,
)

dom.watch()
