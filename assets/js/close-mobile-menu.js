document.addEventListener("click", function(event) {
    if (event.target.classList.contains("navbar-toggler")) {
        document.getElementById("navbarSupportedContent").classList.toggle("show");
    } else if (event.target.classList.contains("anchor-link")) {
        document.getElementById("navbarSupportedContent").classList.remove("show");
        let aria_exp = document.getElementById("navbar-toggler").getAttribute("aria-expanded");
        if (aria_exp === "true")
        {
            aria_exp = "false"
        } else {
            aria_exp = "true"
        }
        document.getElementById("navbar-toggler").setAttribute("aria-expanded", aria_exp);
    }
});
